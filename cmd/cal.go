/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/ranggaadi/ubtool/commands"

	"github.com/spf13/cobra"
)

// calCmd represents the cal command
var calCmd = &cobra.Command{
	Use:   "cal",
	Short: "command used to get academic calendar",
	Long: `
cal command is a command used to to get data about
academic calendar based on ub.ac.id, or filkom.ub.ac.id
if -f / --filkom flags is exist`,
	Run: func(cmd *cobra.Command, args []string) {
		isFil, _ := cmd.Flags().GetBool("filkom")

		if isFil {
			commands.GetKalenderAkademikFilkom()
		} else {
			getCalDemic()
		}
	},
}

func getCalDemic() {
	data := commands.GetKalenderAkademikUB()

	if len(data) == 0 {
		fmt.Println("Something wrong on the server of ub.ac.id, Please try again later")
	}

	for i, tbl := range data {
		table := tablewriter.NewWriter(os.Stdout)
		for j, tuple := range tbl.Data {
			if j == 0 {
				table.SetHeader([]string{tuple.No, tuple.Semester, tuple.Tanggal})
			} else {
				table.Append([]string{tuple.No, tuple.Semester, tuple.Tanggal})
			}
		}
		table.Render()
		if i != len(data)-1 {
			fmt.Println("\n")
		}
	}
}

func init() {
	getCmd.AddCommand(calCmd)
	calCmd.Flags().BoolP("filkom", "f", false, "get academic calendar from filkom: --filkom / -f")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// calCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// calCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
