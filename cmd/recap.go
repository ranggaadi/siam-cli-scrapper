/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/ranggaadi/ubtool/commands"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// recapCmd represents the recap command
var recapCmd = &cobra.Command{
	Use:   "recap",
	Short: "command used to get recapitulation of course taken",
	Long: `
recap command is a subcommand that used with get command
to to get recapitulation of courses taken during college`,

	Run: func(cmd *cobra.Command, args []string) {
		uname, _ := cmd.Flags().GetString("username")

		pass, _ := cmd.Flags().GetString("password")

		if uname == "" || pass == "" {
			uname = viper.GetString("siam_username")
			pass = viper.GetString("siam_password")
		}

		if uname == "" || pass == "" {
			fmt.Println("\nNo username or password is provided")
			return
		}

		getRekapitulasi(uname, pass)
	},
}

func getRekapitulasi(uname, pass string) {
	rekap := commands.GetRekapitulasi(uname, pass)

	if len(rekap.Data) == 0 {
		fmt.Println("\n#########################################################")
		fmt.Println("No data acquired, failed to login or the website is down.")
		fmt.Println("#########################################################")
		return
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"KODE MATA KULIAH", "MATA KULIAH", "JUMLAH SKS", "TAHUN AKADEMIK", "NILAI"})

	for _, v := range rekap.Data {
		table.Append([]string{v.Kode, v.Matkul, fmt.Sprint(v.Sks), v.TahunAkademik, v.Nilai})
	}
	if rekap.GetTotalSks() == 0 {
		fmt.Println("#############################")
		fmt.Println("User currently doesn't have took any courses")
		fmt.Println("#############################")
		return
	}
	table.Append([]string{"", "TOTAL SKS", fmt.Sprint(rekap.GetTotalSks()), "", ""})
	fmt.Println("")
	table.Render()
}

func init() {
	getCmd.AddCommand(recapCmd)
	recapCmd.Flags().StringP("username", "u", "", "Username to login: --username <username> / -u <username>")
	recapCmd.Flags().StringP("password", "p", "", "Password to login: --password <password> / -p <password>")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// recapCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// recapCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
