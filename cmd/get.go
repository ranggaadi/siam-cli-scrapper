/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get the current data specified by the commands and flag",
	Long: `
get command is a parent command used to to get data provided
from official website of University of Brawijaya`,

	Run: func(cmd *cobra.Command, args []string) {
		uname, _ := cmd.Flags().GetString("username")

		pass, _ := cmd.Flags().GetString("password")
		sem, _ := cmd.Flags().GetInt32("semester")
		isShort, _ := cmd.Flags().GetBool("short")

		if uname == "" || pass == "" {
			uname = viper.GetString("siam_username")
			pass = viper.GetString("siam_password")
		}

		if uname == "" || pass == "" {
			fmt.Println("\nNo username or password is provided")
			return
		}

		getNilaiSemester(uname, pass, sem, isShort)
	},
}

func init() {
	rootCmd.AddCommand(getCmd)
	getCmd.Flags().StringP("username", "u", "", "Username to login: --username <username> / -u <username>")
	getCmd.Flags().StringP("password", "p", "", "Password to login: --password <password> / -p <password>")
	getCmd.Flags().Int32P("semester", "s", 0, "Current semester grade: --semester <1..> / -s <1...>")
	getCmd.Flags().Bool("short", false, "Determine short semester or regular semester: --short (if omitted it will considered regular semester)")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")
	// getCmd.Flags().Int32P("semester", "s", 0, "Current semester: --semester 1 / -s 1")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
