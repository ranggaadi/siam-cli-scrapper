/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/ranggaadi/ubtool/commands"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// setCmd represents the set command
var setCmd = &cobra.Command{
	Use:   "set",
	Short: "command used to set a sensitive data",
	Long: `
set command is a command used to to set a sensitive data
like username and password for siam.ub.ac.id, username can 
be set using --username / -u flags, and password can be set
using --password / -p`,

	Run: func(cmd *cobra.Command, args []string) {

		uname, _ := cmd.Flags().GetString("username")

		pass, _ := cmd.Flags().GetString("password")

		if uname == "" || pass == "" {
			fmt.Println("\nNo username or password is provided")
			return
		}

		commands.WriteCUserAndPass(uname, pass)

		viper.SetConfigName("siamub")
		viper.SetConfigType("yaml")
		viper.AddConfigPath("/etc/siamub.yaml")   // path to look for the config file in
		viper.AddConfigPath("$HOME/.siamub.yaml") // call multiple times to add many search paths
		viper.AddConfigPath(".")
		fmt.Println("\nConfiguring username and password success")
	},
}

func init() {
	rootCmd.AddCommand(setCmd)
	setCmd.Flags().StringP("username", "u", "", "Username to login: --username <username> / -u <username>")
	setCmd.Flags().StringP("password", "p", "", "Password to login: --password <password> / -p <password>")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
