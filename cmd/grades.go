/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/ranggaadi/ubtool/commands"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// gradesCmd represents the grades command
var gradesCmd = &cobra.Command{
	Use:   "grades",
	Short: "command used to get current grades",
	Long: `
grades command is a sub command used with get command
to to get course grades provided by siam.ub.ac.id
with semester provided by flag --semester / -s`,
	// Run: func(cmd *cobra.Command, args []string) {
	// 	fmt.Println("grades called")
	// },

	Run: func(cmd *cobra.Command, args []string) {
		uname, _ := cmd.Flags().GetString("username")

		pass, _ := cmd.Flags().GetString("password")
		sem, _ := cmd.Flags().GetInt32("semester")
		isShort, _ := cmd.Flags().GetBool("short")

		if uname == "" || pass == "" {
			uname = viper.GetString("siam_username")
			pass = viper.GetString("siam_password")
		}

		if uname == "" || pass == "" {
			fmt.Println("\nNo username or password is provided")
			return
		}

		getNilaiSemester(uname, pass, sem, isShort)
	},
}

func getNilaiSemester(uname, pass string, sem int32, isShort bool) {
	var binarySelect string
	if isShort {
		binarySelect = "1"
	} else {
		binarySelect = "0"
	}

	indexTables := []*tablewriter.Table{}
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"No", "Kode", "Mata Kuliah", "SKS", "Nilai"})

	currentGrade, currentIndex := commands.GetNilaiSemester(uname, pass, binarySelect, sem)

	if len(currentGrade) == 0 {
		fmt.Println("\n#########################################################")
		fmt.Println("No data acquired, failed to login or the website is down.")
		fmt.Println("#########################################################")
		return
	}

	if len(currentIndex) == 0 {
		fmt.Println("\n#########################################################")
		fmt.Println("No data index acquired, failed to login or the website is down.")
		fmt.Println("#########################################################")
		return
	}

	jumlahSKS := 0
	for _, v := range currentGrade {
		jumlahSKS += v.SKS
		no := fmt.Sprint(v.No)
		SKS := fmt.Sprint(v.SKS)
		table.Append([]string{no, v.Kode, v.MataKuliah, SKS, v.Nilai})
	}
	if jumlahSKS == 0 {
		fmt.Println("#############################")
		fmt.Println("No current semester found.")
		fmt.Println("#############################")
		return
	}
	table.Append([]string{"", "", "JUMLAH SKS", fmt.Sprint(jumlahSKS), ""})

	for _, v := range currentIndex {
		tableIndex := tablewriter.NewWriter(os.Stdout)
		tableIndex.SetHeader([]string{v.Category, "", "", ""})
		tableIndex.Append([]string{"", "IP", "SKS", "MK"})
		for _, data := range v.Data {
			tableIndex.Append([]string{data.Category, data.IP, data.Sks, data.MK})
		}

		indexTables = append(indexTables, tableIndex)
	}

	fmt.Println("")
	table.Render()

	for _, table := range indexTables {
		fmt.Println("")
		table.Render()
	}
}

func init() {
	getCmd.AddCommand(gradesCmd)
	gradesCmd.Flags().StringP("username", "u", "", "Username to login: --username <username> / -u <username>")
	gradesCmd.Flags().StringP("password", "p", "", "Password to login: --password <password> / -p <password>")
	gradesCmd.Flags().Int32P("semester", "s", 0, "Current semester grade: --semester <1..> / -s <1...>")
	gradesCmd.Flags().Bool("short", false, "Determine short semester or regular semester: --short (if omitted it will considered regular semester)")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gradesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// gradesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
