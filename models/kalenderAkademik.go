package models

type TupleKalender struct {
	No       string
	Semester string
	Tanggal  string
}

type KalenderAkademik struct {
	Data []*TupleKalender
}
