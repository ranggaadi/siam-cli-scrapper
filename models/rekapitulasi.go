package models

type TupleRekap struct {
	Kode          string
	Matkul        string
	Sks           int
	TahunAkademik string
	Nilai         string
}

type Rekapitulasi struct {
	Data []*TupleRekap
}

func (r *Rekapitulasi) GetTotalSks() int {
	sum := 0
	for _, v := range r.Data {
		sum += v.Sks
	}
	return sum
}
