package models

type NilaiSemester struct {
	No         int
	Kode       string
	MataKuliah string
	SKS        int
	Nilai      string
}
