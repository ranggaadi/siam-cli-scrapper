package models

type Index struct {
	Category string //lulus or beban
	IP       string
	Sks      string
	MK       string
}

type IndexPrestasi struct {
	Category string // semester or kumulatif
	Data     []*Index
}
