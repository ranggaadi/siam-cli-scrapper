package commands

import (
	"fmt"
	"io/ioutil"
	"os/user"
	"runtime"
)

func WriteCUserAndPass(uname, pass string) {
	confFile := []byte(fmt.Sprintf("---\nsiam_username: %s\nsiam_password: %s\n", uname, pass))
	if runtime.GOOS == "linux" {
		err := ioutil.WriteFile("/etc/siamub.yaml", confFile, 0777)
		errHandler(err)
	} else if runtime.GOOS == "windows" {
		userInfo, err := user.Current()
		homedir := userInfo.HomeDir
		err = ioutil.WriteFile(fmt.Sprintf("%s\\.siamub.yaml", homedir), confFile, 0644)
		errHandler(err)
	}
}

func errHandler(e error) {
	if e != nil {
		panic(e)
	}
}
