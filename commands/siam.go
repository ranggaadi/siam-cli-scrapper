package commands

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/briandowns/spinner"
	"github.com/ranggaadi/ubtool/models"

	"github.com/gocolly/colly"
)

var c = colly.NewCollector(colly.AllowedDomains("siam.ub.ac.id", "filkom.ub.ac.id", "ub.ac.id"))
var spin *spinner.Spinner

func GetNilaiSemester(uname, pass, isShort string, sem int32) ([]models.NilaiSemester, []models.IndexPrestasi) {
	var allInfo []models.NilaiSemester
	var allIndex []models.Index
	var indexInfo []models.IndexPrestasi
	var flag int
	var loginFlag bool
	var latestSem string
	var sems string

	c.OnHTML("small.error-code", func(e *colly.HTMLElement) {
		if !loginFlag {
			if strings.Contains(e.DOM.Text(), "Silahkan") {
				spin.Stop()
				fmt.Println("\n Failed to login: username or password incorrect")
				loginFlag = true
				return
			}
		}
	})

	if loginFlag {
		return nil, nil
	}

	c.OnHTML("form.form-login.right[action=\"index.php\"]", func(el *colly.HTMLElement) {
		if loginFlag {
			return
		}

		spin = spinner.New(spinner.CharSets[11], 100*time.Millisecond, spinner.WithWriter(os.Stdout))
		spin.Prefix = " Logging in ... "
		spin.Start()

		err := c.Post("https://siam.ub.ac.id/index.php", map[string]string{
			"status_loc": "Unable to retrieve your location",
			"lat":        "",
			"long":       "",
			"username":   uname,
			"password":   pass,
			"login":      "Masuk"})

		if err != nil {
			log.Fatalf("error while trying to login: %v", err)
		}

		el.Request.Visit("https://siam.ub.ac.id/khs.php")
	})

	c.OnHTML("select[name=\"smtView\"]", func(e *colly.HTMLElement) {
		latestSem, _ = e.DOM.Children().First().Attr("value")
	})
	if sem <= 0 {
		sems = latestSem
	} else {
		sems = fmt.Sprint(sem)
	}

	c.OnHTML("form[action=\"khs.php\"][method=\"post\"]", func(el *colly.HTMLElement) {
		spin.Stop()
		spin.Prefix = "Selecting semester ... "
		spin.Start()
		if flag > 0 {
			return
		}
		flag++

		err := c.Post("https://siam.ub.ac.id/khs.php", map[string]string{
			"smtView":   sems,
			"is_pendek": isShort,
			"view":      "  TAMPIL  "})

		if err != nil {
			log.Fatalf("error while trying to login: %v", err)
		}
	})

	c.OnHTML(fmt.Sprint("select[name=\"smtView\"] option[value=\"%s\"][selected]", sems), func(e *colly.HTMLElement) {
		flag = 1
	})

	c.OnHTML("table[width=\"515\"]", func(e *colly.HTMLElement) {
		spin.Stop()
		spin.Prefix = "Scrapping data ... "
		spin.Start()

		if flag <= 0 {
			return
		}

		// flag--
		e.ForEach("tr[class=\"text\"][bgcolor=\"#ffffff\"]", func(i int, e *colly.HTMLElement) {
			if strings.Contains(e.ChildText("td:nth-of-type(3)"), "JUMLAH") {
				return
			}
			var info models.NilaiSemester
			no, err := strconv.Atoi(e.ChildText("td:first-of-type"))
			if err != nil {
				log.Fatalf("Cannot converting No column to a integer: %v", err)
			}
			sks, err := strconv.Atoi(e.ChildText("td:nth-of-type(4)"))
			if err != nil {
				log.Fatalf("Cannot converting SKS column to a integer: %v", err)
			}

			info.No = no
			info.Kode = e.ChildText("td:nth-of-type(2)")
			info.MataKuliah = e.ChildText("td:nth-of-type(3)")
			info.SKS = sks
			info.Nilai = e.ChildText("td:nth-of-type(5)")

			allInfo = append(allInfo, info)
		})
	})

	c.OnHTML("table[cellspacing=\"0\"][cellpadding=\"0\"][border=\"0\"]:nth-of-type(3):last-of-type", func(e *colly.HTMLElement) {
		spin.Stop()

		if flag <= 0 {
			return
		}

		flag--
		e.ForEach("tr.text", func(i int, e *colly.HTMLElement) {
			var indeks models.Index

			if strings.Contains(e.ChildText("td:nth-of-type(3)"), "Lulus") {
				indeks.Category = "Lulus"
			} else if strings.Contains(e.ChildText("td:nth-of-type(3)"), "Beban") {
				indeks.Category = "Beban"
			}

			indeks.IP = strings.ReplaceAll(e.ChildText("td:nth-of-type(4)"), ": ", "")
			indeks.Sks = strings.ReplaceAll(e.ChildText("td:nth-of-type(6)"), ": ", "")
			indeks.MK = strings.ReplaceAll(e.ChildText("td:nth-of-type(8)"), ": ", "")

			allIndex = append(allIndex, indeks)
		})

		for i := 0; i < len(allIndex)/2; i++ {
			currentIndex := models.IndexPrestasi{}

			if i == 1 {
				currentIndex.Category = "KUMULATIF"
				currentIndex.Data = []*models.Index{&allIndex[i+1], &allIndex[i+2]}
			} else {
				currentIndex.Category = "SEMESTER"
				currentIndex.Data = []*models.Index{&allIndex[i], &allIndex[i+1]}
			}

			indexInfo = append(indexInfo, currentIndex)
		}
	})

	c.Visit("https://siam.ub.ac.id")
	return allInfo, indexInfo
}

func GetRekapitulasi(uname, pass string) models.Rekapitulasi {
	var rekap models.Rekapitulasi
	var loginFlag bool

	c.OnHTML("small.error-code", func(e *colly.HTMLElement) {
		if !loginFlag {
			if strings.Contains(e.DOM.Text(), "Silahkan") {
				spin.Stop()
				fmt.Println("\n Failed to login: username or password incorrect")
				loginFlag = true
				return
			}
		}
	})

	if loginFlag {
		return models.Rekapitulasi{}
	}

	c.OnHTML("form.form-login.right[action=\"index.php\"]", func(el *colly.HTMLElement) {

		if loginFlag {
			return
		}

		spin = spinner.New(spinner.CharSets[11], 100*time.Millisecond, spinner.WithWriter(os.Stdout))
		spin.Prefix = " Logging in ... "
		spin.Start()

		err := c.Post("https://siam.ub.ac.id/index.php", map[string]string{
			"status_loc": "Unable to retrieve your location",
			"lat":        "",
			"long":       "",
			"username":   uname,
			"password":   pass,
			"login":      "Masuk"})

		if err != nil {
			log.Fatalf("error while trying to login: %v", err)
		}

		el.Request.Visit("https://siam.ub.ac.id/rekapstudi.php")
	})

	c.OnHTML("table[cellspacing=\"1\"][cellpadding=\"2\"][border=\"0\"]", func(e *colly.HTMLElement) {
		spin.Stop()
		spin.Prefix = "Scrapping data ... "
		spin.Start()

		e.ForEach("tr[class=\"text\"][bgcolor=\"#f7f7f7\"]", func(i int, e *colly.HTMLElement) {
			var tuple models.TupleRekap
			sks, err := strconv.Atoi(e.ChildText("td:nth-of-type(3)"))
			if err != nil {
				log.Fatalf("Cannot converting SKS column to a integer: %v", err)
			}

			tuple.Kode = e.ChildText("td:first-of-type")
			tuple.Matkul = e.ChildText("td:nth-of-type(2)")
			tuple.Sks = sks
			tuple.TahunAkademik = e.ChildText("td:nth-of-type(4)")
			tuple.Nilai = e.ChildText("td:nth-of-type(5)")

			spin.Stop()
			rekap.Data = append(rekap.Data, &tuple)
		})
	})

	c.Visit("https://siam.ub.ac.id")
	return rekap
}

func GetKalenderAkademikUB() []*models.KalenderAkademik {
	var data []*models.KalenderAkademik

	spin = spinner.New(spinner.CharSets[11], 100*time.Millisecond, spinner.WithWriter(os.Stdout))
	spin.Prefix = " Scrapping Calendar ... "
	spin.Start()

	c.OnHTML("table[border=\"1\"]", func(e *colly.HTMLElement) {
		var table models.KalenderAkademik
		e.ForEach("tr", func(i int, e *colly.HTMLElement) {
			var info models.TupleKalender
			info.No = e.ChildText("td:first-of-type")
			info.Semester = e.ChildText("td:nth-of-type(2)")
			info.Tanggal = e.ChildText("td:nth-of-type(3)")
			table.Data = append(table.Data, &info)
		})
		data = append(data, &table)
	})

	spin.Stop()
	c.Visit("https://ub.ac.id/id/academic/academic-calendar/")
	return data
}

func GetKalenderAkademikFilkom() {

	spin = spinner.New(spinner.CharSets[11], 100*time.Millisecond, spinner.WithWriter(os.Stdout))
	spin.Prefix = " Scrapping Calendar Link ... "
	spin.Start()

	fmt.Println()
	c.OnHTML("section[itemprop=\"articleBody\"]", func(e *colly.HTMLElement) {
		e.ForEach("p[style=\"text-align:left\"]", func(i int, e *colly.HTMLElement) {
			e.ForEach("a[target=\"_blank\"]", func(i int, e *colly.HTMLElement) {
				fmt.Println(e.DOM.Parent().Text())
				fmt.Println(e.Attr("href"))
				fmt.Println()
			})
		})
	})
	spin.Stop()
	c.Visit("https://filkom.ub.ac.id/page/read/academic-calendar/26b50f8d0d22e9")
}
